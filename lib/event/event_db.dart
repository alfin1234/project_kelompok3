import 'dart:convert';
import 'package:get/get.dart';
import 'package:project_kel_3/config/api.dart';
import 'package:project_kel_3/event/event_pref.dart';
import 'package:project_kel_3/model/barang.dart';
import 'package:project_kel_3/model/pengajuan.dart';
import 'package:project_kel_3/model/pengajuandetail.dart';
import 'package:project_kel_3/model/pengembalian.dart';
import 'package:project_kel_3/model/pengembaliandetail.dart';
import 'package:project_kel_3/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:project_kel_3/screen/login.dart';
import 'package:project_kel_3/widget/info.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              Login(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Barang
  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['barang'];

          barang.forEach((barang) {
            listBarang.add(Barang.fromJson(barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }
  //AddBarang

  static Future<String> AddBarang(String kodeBarang, String namaBarang,
      String jumlahBarang) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kodeBarang': kodeBarang,
        'namaBarang': namaBarang,
        'jumlahBarang': jumlahBarang,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Barang
    static Future<void> UpdateBarang(String kodeBarang, String namaBarang,
      String jumlahBarang) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kodeBarang':kodeBarang ,
        'namaBarang': namaBarang,
        'jumlahBarang': jumlahBarang,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Barang');
        } else {
          Info.snackbar('Gagal Update Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //delete barang
    static Future<void> deleteBarang(String kodeBarang)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteBarang), body: {'kodeBarang': kodeBarang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Barang');
        } else {
          Info.snackbar('Gagal Delete Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Pengajuan
  static Future<List<Pengajuan>> getPengajuan() async {
    List<Pengajuan> listPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuan = responBody['pengajuan'];

          pengajuan.forEach((pengajuan) {
            listPengajuan.add(Pengajuan.fromJson(pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuan;
  }
  //AddPengajuan

  static Future<String> AddPengajuan(String kodePengajuan, String tglPengajuan,
      String npmPeminjam, String namaPeminjam, String mhsProdi, String noHandphone) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuan), body: {
        'kodePengajuan': kodePengajuan,
        'tglPengajuan': tglPengajuan,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'mhsProdi': mhsProdi,
        'noHandphone' : noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Pengajuan
    static Future<void> UpdatePengajuan(String kodePengajuan, String tglPengajuan,
      String npmPeminjam, String namaPeminjam, String mhsProdi, String noHandphone) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuan), body: {
         'kodePengajuan': kodePengajuan,
        'tglPengajuan': tglPengajuan,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'mhsProdi': mhsProdi,
        'noHandphone' : noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuan');
        } else {
          Info.snackbar('Gagal Update Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //delete pengajuan
    static Future<void> deletePengajuan(String kodePengajuan)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengajuan), body: {'kodePengajuan': kodePengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //Pengembalian
  static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['pengembalian'];

          pengembalian.forEach((pengembalian) {
            listPengembalian.add(Pengembalian.fromJson(pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }
  //AddPengembalian

  static Future<String> AddPengembalian(String kodePengembalian, String kodePengajuan,
      String tglKembali) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kodePengembalian': kodePengembalian,
        'kodePengajuan': kodePengajuan,
        'tglKembali': tglKembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Pengembalian
    static Future<void> UpdatePengembalian(String kodePengembalian, String kodePengajuan,
      String tglKembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'kodePengembalian':kodePengembalian ,
        'kodePengajuan': kodePengajuan,
        'tglKembali': tglKembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //delete pengembalian
    static Future<void> deletePengembalian(String kodePengembalian)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengembalian), body: {'kodePengembalian': kodePengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Pengajuandetail
  static Future<List<Pengajuandetail>> getPengajuandetail() async {
    List<Pengajuandetail> listPengajuandetail = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuandetail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuandetail = responBody['pengajuandetail'];

          pengajuandetail.forEach((pengajuandetail) {
            listPengajuandetail.add(Pengajuandetail.fromJson(pengajuandetail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuandetail;
  }
  //AddPengajuandetail

  static Future<String> AddPengajuandetail(String kodePengajuan, String kodeBarang,
      String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuandetail), body: {
        'kodePengajuan': kodePengajuan,
        'kodeBarang': kodeBarang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengajuandetail Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Pengajuan
    static Future<void> UpdatePengajuandetail(String kodePengajuan, String kodeBarang,
      String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuandetail), body: {
         'kodePengajuan': kodePengajuan,
        'kodeBarang': kodeBarang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuandetail');
        } else {
          Info.snackbar('Gagal Update Pengajuandetail');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //delete pengajuan
    static Future<void> deletePengajuandetail(String kodePengajuan)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengajuandetail), body: {'kodePengajuan': kodePengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengajuandetail');
        } else {
          Info.snackbar('Gagal Delete Pengajuandetail');
        }
      }
    } catch (e) {
      print(e);
    }
  }
   //Pengembaliandetail
  static Future<List<Pengembaliandetail>> getPengembaliandetail() async {
    List<Pengembaliandetail> listPengembaliandetail = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembaliandetail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembaliandetail = responBody['pengembaliandetail'];

          pengembaliandetail.forEach((pengembaliandetail) {
            listPengembaliandetail.add(Pengembaliandetail.fromJson(pengembaliandetail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembaliandetail;
  }
  //AddPengembalian

  static Future<String> AddPengembaliandetail(String kodePengembalian, String kodeBarang,
      String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembaliandetail), body: {
        'kodePengembalian': kodePengembalian,
        'kodeBarang': kodeBarang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembaliandetail Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Pengembalian
    static Future<void> UpdatePengembaliandetail(String kodePengembalian, String kodeBarang,
      String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembaliandetail), body: {
        'kodePengembalian':kodePengembalian ,
        'kodeBarang': kodeBarang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembaliandetail');
        } else {
          Info.snackbar('Gagal Update Pengembaliandetail');
        }
      }
    } catch (e) {
      print(e);
    }
  }
  //delete pengembalian
    static Future<void> deletePengembaliandetail(String kodePengembalian)async {
    try {
      var response = await http
          .post(Uri.parse(Api.deletePengembaliandetail), body: {'kodePengembalian': kodePengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembaliandetail');
        } else {
          Info.snackbar('Gagal Delete Pengembaliandetail');
        }
      }
    } catch (e) {
      print(e);
    }
  }

}
