import 'package:get/get.dart';
import 'package:project_kel_3/model/pengembaliandetail.dart';

class CPengembaliandetail extends GetxController {
  Rx<Pengembaliandetail> _pengembaliandetail = Pengembaliandetail().obs;

  Pengembaliandetail get user => _pengembaliandetail.value;

  void setUser(Pengembaliandetail dataPengembaliandetail) => _pengembaliandetail.value = dataPengembaliandetail;
}
