import 'package:get/get.dart';
import 'package:project_kel_3/model/pengajuandetail.dart';

class CPengajuandetail extends GetxController {
  Rx<Pengajuandetail> _pengajuandetail = Pengajuandetail().obs;

  Pengajuandetail get user => _pengajuandetail.value;

  void setUser(Pengajuandetail dataPengajuandetail) => _pengajuandetail.value = dataPengajuandetail;
}
