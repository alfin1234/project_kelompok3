class Barang {
  String? kodeBarang;
  String? namaBarang;
  String? jumlahBarang;

  Barang({
    this.kodeBarang,
    this.namaBarang,
    this.jumlahBarang,
  });

  factory Barang.fromJson(Map<String, dynamic> json) => Barang(
        kodeBarang: json['kodeBarang'],
        namaBarang: json['namaBarang'],
        jumlahBarang: json['jumlahBarang'],
      );

  Map<String, dynamic> toJson() => {
        'kodeBarang': this.kodeBarang,
        'namaBarang': this.namaBarang,
        'jumlahBarang': this.jumlahBarang,
      };
}
