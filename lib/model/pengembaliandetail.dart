class Pengembaliandetail {
  String? kodePengembalian;
  String? kodeBarang;
  String? jumlah;

  Pengembaliandetail({
    this.kodePengembalian,
    this.kodeBarang,
    this.jumlah,
  });

  factory Pengembaliandetail.fromJson(Map<String, dynamic> json) => Pengembaliandetail(
        kodePengembalian: json['kodePengembalian'],
        kodeBarang: json['kodeBarang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kodePengembalian': this.kodePengembalian,
        'kodeBarang': this.kodeBarang,
        'jumlah': this.jumlah,
      };
}
