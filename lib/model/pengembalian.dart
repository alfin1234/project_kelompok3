class Pengembalian {
  String? kodePengembalian;
  String? kodePengajuan;
  String? tglKembali;

  Pengembalian({
    this.kodePengembalian,
    this.kodePengajuan,
    this.tglKembali,
  });

  factory Pengembalian.fromJson(Map<String, dynamic> json) => Pengembalian(
        kodePengembalian: json['kodePengembalian'],
        kodePengajuan: json['kodePengajuan'],
        tglKembali: json['tglKembali'],
      );

  Map<String, dynamic> toJson() => {
        'kodePengembalian': this.kodePengembalian,
        'kodePengajuan': this.kodePengajuan,
        'tglKembali': this.tglKembali,
      };
}
