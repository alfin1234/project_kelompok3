class Pengajuandetail {
  String? kodePengajuan;
  String? kodeBarang;
  String? jumlah;

  Pengajuandetail({
    this.kodePengajuan,
    this.kodeBarang,
    this.jumlah,
  });

  factory Pengajuandetail.fromJson(Map<String, dynamic> json) => Pengajuandetail(
        kodePengajuan: json['kodePengajuan'],
        kodeBarang: json['kodeBarang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kodePengajuan': this.kodePengajuan,
        'kodeBarang': this.kodeBarang,
        'jumlah': this.jumlah,
      };
}
