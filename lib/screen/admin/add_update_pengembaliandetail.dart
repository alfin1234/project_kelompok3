import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel_3/config/asset.dart';
import 'package:project_kel_3/event/event_db.dart';
import 'package:project_kel_3/screen/admin/list_pengembaliandetail.dart';
import 'package:project_kel_3/widget/info.dart';

import '../../model/pengembaliandetail.dart';

class AddUpdatePengembaliandetail extends StatefulWidget {
  final Pengembaliandetail? pengembaliandetail;
  AddUpdatePengembaliandetail({this.pengembaliandetail});

  @override
  State<AddUpdatePengembaliandetail> createState() => _AddUpdatePengembaliandetailState();
}

class _AddUpdatePengembaliandetailState extends State<AddUpdatePengembaliandetail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerpb = TextEditingController();
  var _controllerbrg = TextEditingController();
  var _controllerjmlh = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembaliandetail != null) {
      _controllerpb.text = widget.pengembaliandetail!.kodePengembalian!;
      _controllerbrg.text = widget.pengembaliandetail!.kodeBarang!;
      _controllerjmlh.text = widget.pengembaliandetail!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembaliandetail != null
            ? Text('Update Pengembaliandetail')
            : Text('Tambah Pengembaliandetail'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembaliandetail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerpb,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerbrg,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjmlh,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembaliandetail == null) {
                        String message = await EventDb.AddPengembaliandetail(
                          _controllerpb.text,
                          _controllerbrg.text,
                          _controllerjmlh.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerpb.clear();
                          _controllerbrg.clear();
                          _controllerjmlh.clear();
                          Get.off(
                            ListPengembaliandetail(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembaliandetail(
                          _controllerpb.text,
                          _controllerbrg.text,
                          _controllerjmlh.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengembaliandetail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
