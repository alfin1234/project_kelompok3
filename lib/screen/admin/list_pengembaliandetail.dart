import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel_3/config/asset.dart';
import 'package:project_kel_3/event/event_db.dart';
import 'package:project_kel_3/model/pengembaliandetail.dart';
import 'package:project_kel_3/screen/admin/add_update_pengembalian.dart';
import 'package:project_kel_3/screen/admin/add_update_pengembaliandetail.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembaliandetail.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembaliandetail.dart';

import '../../model/pengembaliandetail.dart';

class ListPengembaliandetail extends StatefulWidget {
  @override
  State<ListPengembaliandetail> createState() => _ListPengembaliandetailState();
}

class _ListPengembaliandetailState extends State<ListPengembaliandetail> {
  List<Pengembaliandetail> _listPengembaliandetail = [];

  void getPengembaliandetail() async {
    _listPengembaliandetail = await EventDb.getPengembaliandetail();

    setState(() {});
  }

  @override
  void initState() {
    getPengembaliandetail();
    super.initState();
  }

  void showOption(Pengembaliandetail? pengembaliandetail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembaliandetail(pengembaliandetail: pengembaliandetail))
            ?.then((value) => getPengembaliandetail());
        break;
      case 'delete':
        EventDb.deletePengembaliandetail(pengembaliandetail!.kodePengembalian!)
            .then((value) => getPengembaliandetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Pengembaliandetail'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPengembaliandetail.length > 0
              ? ListView.builder(
                  itemCount: _listPengembaliandetail.length,
                  itemBuilder: (context, index) {
                    Pengembaliandetail pengembaliandetail = _listPengembaliandetail[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengembaliandetail.kodePengembalian ?? ''),
                      subtitle: Text(pengembaliandetail.kodeBarang ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengembaliandetail),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengembaliandetail())?.then((value) => getPengembaliandetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
