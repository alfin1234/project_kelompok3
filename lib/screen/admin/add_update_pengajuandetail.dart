import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel_3/config/asset.dart';
import 'package:project_kel_3/event/event_db.dart';
import 'package:project_kel_3/screen/admin/list_pengajuandetail.dart';
import 'package:project_kel_3/widget/info.dart';

import '../../model/pengajuandetail.dart';

class AddUpdatePengajuandetail extends StatefulWidget {
  final Pengajuandetail? pengajuandetail;
  AddUpdatePengajuandetail({this.pengajuandetail});

  @override
  State<AddUpdatePengajuandetail> createState() => _AddUpdatePengajuandetailState();
}

class _AddUpdatePengajuandetailState extends State<AddUpdatePengajuandetail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodepj = TextEditingController();
  var _controllerbrg = TextEditingController();
  var _controllerjmlh = TextEditingController();


  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengajuandetail != null) {
      _controllerkodepj.text = widget.pengajuandetail!.kodePengajuan!;
      _controllerbrg.text = widget.pengajuandetail!.kodeBarang!;
      _controllerjmlh.text = widget.pengajuandetail!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengajuandetail != null
            ? Text('Update Pengajuandetail')
            : Text('Tambah Pengajuandetail'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengajuandetail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodepj,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerbrg,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjmlh,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengajuandetail == null) {
                        String message = await EventDb.AddPengajuandetail(
                          _controllerkodepj.text,
                          _controllerbrg.text,
                          _controllerjmlh.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodepj.clear();
                          _controllerbrg.clear();
                          _controllerjmlh.clear();
                          Get.off(
                            ListPengajuandetail(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuandetail(
                          _controllerkodepj.text,
                          _controllerbrg.text,
                          _controllerjmlh.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengajuandetail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
