import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel_3/config/asset.dart';
import 'package:project_kel_3/event/event_db.dart';
import 'package:project_kel_3/model/pengajuandetail.dart';
import 'package:project_kel_3/screen/admin/add_update_pengajuandetail.dart';
// import 'package:project_kelas/screen/admin/add_update_pengajuandetail.dart';

import '../../model/pengajuandetail.dart';

class ListPengajuandetail extends StatefulWidget {
  @override
  State<ListPengajuandetail> createState() => _ListPengajuandetailState();
}

class _ListPengajuandetailState extends State<ListPengajuandetail> {
  List<Pengajuandetail> _listPengajuandetail = [];

  void getPengajuandetail() async {
    _listPengajuandetail = await EventDb.getPengajuandetail();

    setState(() {});
  }

  @override
  void initState() {
    getPengajuandetail();
    super.initState();
  }

  void showOption(Pengajuandetail? pengajuandetail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuandetail(pengajuandetail: pengajuandetail))
            ?.then((value) => getPengajuandetail());
        break;
      case 'delete':
        EventDb.deletePengajuandetail(pengajuandetail!.kodePengajuan!)
            .then((value) => getPengajuandetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Pengajuandetail'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPengajuandetail.length > 0
              ? ListView.builder(
                  itemCount: _listPengajuandetail.length,
                  itemBuilder: (context, index) {
                    Pengajuandetail pengajuandetail = _listPengajuandetail[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengajuandetail.kodePengajuan ?? ''),
                      subtitle: Text(pengajuandetail.kodeBarang ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengajuandetail),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengajuandetail())?.then((value) => getPengajuandetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
