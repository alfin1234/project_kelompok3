import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel_3/config/asset.dart';
import 'package:project_kel_3/event/event_db.dart';
import 'package:project_kel_3/screen/admin/list_pengembalian.dart';
import 'package:project_kel_3/widget/info.dart';

import '../../model/pengembalian.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? pengembalian;
  AddUpdatePengembalian({this.pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerpengembalian = TextEditingController();
  var _controllerpengajuan = TextEditingController();
  var _controllertgl = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian != null) {
      _controllerpengembalian.text = widget.pengembalian!.kodePengembalian!;
      _controllerpengajuan.text = widget.pengembalian!.kodePengajuan!;
      _controllertgl.text = widget.pengembalian!.tglKembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembalian != null
            ? Text('Update Pengembalian')
            : Text('Tambah Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerpengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerpengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertgl,
                  decoration: InputDecoration(
                      labelText: "Tanggal Kembali",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian == null) {
                        String message = await EventDb.AddPengembalian(
                          _controllerpengembalian.text,
                          _controllerpengajuan.text,
                          _controllertgl.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerpengembalian.clear();
                          _controllerpengajuan.clear();
                          _controllertgl.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                          _controllerpengembalian.text,
                          _controllerpengajuan.text,
                          _controllertgl.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
